var codigo =(function(){
    var plantilla_anime = function(descripcion, rutaimg, link, titulo, caps, servidor){

        var contenedor = document.getElementById('contenedor');
        
        var divcol = document.createElement('div');
        divcol.setAttribute('class', 'col s12 m7');

        var divcarh = document.createElement('div');
        divcarh.setAttribute('class', 'card horizontal');

        var divcarimg = document.createElement('div');
        divcarimg.setAttribute('class', 'card-img');

        imagen = document.createElement('img');
        imagen.setAttribute('src', rutaimg);
        imagen.setAttribute('width', '369px');
        imagen.setAttribute('height', '500px');

        //width="100px" height=""

        divcarimg.appendChild(imagen); //agregar a card horizontal


        var divcardtacked = document.createElement('div');
        divcardtacked.setAttribute('class', 'card-stacked');

        
        var cardcontent = document.createElement('div');
        cardcontent.setAttribute('class', 'card-content');

        var h2 = document.createElement('h2');
        h2.textContent = titulo;

        cardcontent.appendChild(h2);

        var desc = document.createElement('p');
        desc.setAttribute('class', 'desc');
        desc.textContent = descripcion;

        cardcontent.appendChild(desc);

        var lista = document.createElement('ul');
        lista.setAttribute('class', 'desc');

        var title = "Título: ";
        var nocaps = "Capítulos: ";
        var server = "Servidor: ";

        var listelem1 = document.createElement('li');
        listelem1.textContent = title + titulo;
        var listelem2 = document.createElement('li');
        listelem2.textContent = nocaps + caps;
        var listelem3 = document.createElement('li');
        listelem3.textContent = server + servidor;

        lista.appendChild(listelem1);
        lista.appendChild(listelem2);
        lista.appendChild(listelem3);

        divcardtacked.appendChild(cardcontent);
        divcardtacked.appendChild(lista);

        divcarh.appendChild(divcarimg);
        divcarh.appendChild(divcardtacked);
        

        var cardaction = document.createElement('div');
        cardaction.setAttribute('class', 'card-action');

        var divmega = document.createElement('div');
        divmega.setAttribute('class', 'mega');

        var a = document.createElement('a');
        a.setAttribute('href', link);
        a.setAttribute('target', '_blank');
        a.textContent = 'DESCARGA MEGA';

        divmega.appendChild(a);

        cardaction.appendChild(divmega);

        divcol.appendChild(divcarh);
        divcol.appendChild(cardaction);

        contenedor.appendChild(divcol);
    };

    var barraIndividual = function(){
        var elementos = ['anime/', 'manga/', 'series/', 'peliculas/', 'musica/', 'software/'];
        var cadenas = ['Anime', 'Manga', 'Series', 'Películas', 'Música', 'Software'];

        var barra = document.getElementById('barra');

        var divwrapper = document.createElement('div');
        divwrapper.setAttribute('class', 'nav-wrapper');

        var a = document.createElement('a');
        a.setAttribute('class', 'brand-logo');
        a.setAttribute('href', '../../../');
        a.textContent='logo prron';

        divwrapper.appendChild(a);

        var lista = document.createElement('ul');
        lista.setAttribute('id', 'nav-movile');
        lista.setAttribute('class', 'right hide-on-med-and-down');

        for(var i=0; i<elementos.length; i++){
            var listelem = document.createElement('li');
            var enlace = document.createElement('a');
            enlace.setAttribute('href', '../../../content/'+elementos[i]);
            enlace.textContent = cadenas[i];
            listelem.appendChild(enlace);
            lista.appendChild(listelem);
        }

        divwrapper.appendChild(lista);
        barra.appendChild(divwrapper);

    };

    var barraCategoria = function(){
        var elementos = ['anime/', 'manga/', 'series/', 'peliculas/', 'musica/', 'software/'];
        var cadenas = ['Anime', 'Manga', 'Series', 'Películas', 'Música', 'Software'];

        var barra = document.getElementById('barra');

        var divwrapper = document.createElement('div');
        divwrapper.setAttribute('class', 'nav-wrapper');

        var a = document.createElement('a');
        a.setAttribute('class', 'brand-logo');
        a.setAttribute('href', '../../');
        a.textContent='logo prron';

        divwrapper.appendChild(a);

        var lista = document.createElement('ul');
        lista.setAttribute('id', 'nav-movile');
        lista.setAttribute('class', 'right hide-on-med-and-down');

        for(var i=0; i<elementos.length; i++){
            var listelem = document.createElement('li');
            var enlace = document.createElement('a');
            enlace.setAttribute('href', '../../content/'+elementos[i]);
            enlace.textContent = cadenas[i];
            listelem.appendChild(enlace);
            lista.appendChild(listelem);
        }

        divwrapper.appendChild(lista);
        barra.appendChild(divwrapper);

    };



    return{
        'plantilla_anime': plantilla_anime,
        'barraIndividual': barraIndividual,
        'barraCategoria': barraCategoria
    }

})();